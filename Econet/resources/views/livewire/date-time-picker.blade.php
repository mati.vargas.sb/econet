{ 
    <div class="space-y-4 rounded-md">
    <div class="max-w-2xl bg-gray-300 text-center rounded-md">
        <input 
        type="date" 
        id="date" 
        class="border-gray-100 rounded-sm focus:outline" 
        autocomplete="off" 
        wire:model="date">
    </div>

    <div class="grid grid-cols-6 gap-4">
        @foreach ($availableTimes as $key => $time /* buscar donde estan estos parametros */)
            <div class="w-full group">
                <input 
                type="radio" --nose que es --
                id="interval-{{ $key }}" 
                name="time"
                value="{{ $date . ' ' . $key }}" 
                class="hidden peer">
                <label class="inline-block w-full text-center border px-8 py-2 rounded peer-checked:bg-green-400 peer-checker:border-none" 
                for="interval-{{ $key }}"
                wire:key="interval-{{ $key }}">
                    {{ $key }}
                </label>
            </div>
        @endforeach

    </div>
</div>

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/pikaday/1.8.2/pikaday.min.js" 
integrity="sha512-dkMzAitT+RFxzaHsyXAd1KtYpmuP/Jl6yOPYUu1s20dLfizq6cpbzDFNSAANb3IZbyhVhAbZxAyeqORpjkF3oQ==" 
crossorigin="anonymous" 
referrerpolicy="no-referrer"></script>
<script>
    new Pikaday ({
        field : document.getElementById('date'),
        onSelect: function() {
            @this.set('date', this.getMoment().format('YYYY-MM-DD'));
        }
    })
</script>

@endpush
