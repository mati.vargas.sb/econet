<div>
    <div class="relative max-w-xl">
        <div class="filter blur-xl mix-blend-multiply absolute top-0 left-8 w-80 h-80 bg-green-500 rounded-full animate-blob"></div>
        <div class="filter blur-xl mix-blend-multiply absolute top-0 left-52 w-80 h-80 bg-orange-200 rounded-full animate-blob animation-delay-2000"></div>
        <div class="filter blur-xl mix-blend-multiply absolute left-28 top-28 w-80 h-80 bg-blue-400 rounded-full animate-blob animation-delay-4000"></div>
    </div>
</div>