<div class="rounded-t-xl">
    <header class="mx-auto w-full px-20 border-b border-zinc-200 ">
        {{-- DESKTOP MENU --}}
        <section class="mx-auto flex justify-between items-center">

            {{-- logo --}}
            <a href="{{ route('home') }}"><img src="/images/logo_econet.png" alt="logo" class="w-20 h-20"></a>

            {{-- nav-bar --}}
            <div>
                <button id="hambuger-button" class="text-3xl lg:hidden cursor-pointer">
                    &#9776;
                </button>
                <nav class="hidden lg:flex space-x-40 text-sm text-zinc-900">
                    <a href="{{ route('posts.index') }}">Nos prestations</a>
                    <a href="{{ route('appointments.create') }}">Rendez-vous</a>
                    <a href="">Contact</a>
                </nav>
            </div>

            {{-- compte --}}
            <x-account />
        </section>

        {{-- MOBILE MENU --}}
        <section id="mobile-menu">
        </section>
    </header>
</div>
