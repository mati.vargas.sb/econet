<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>

<body>
    <div class="mx-auto w-4/8 py-24">
        <div class="text-center">
            <h1 class="text-5xl uppercase bold">
                Update post
            </h1>
        </div>


        <div class="flex justify-center pt-20">
            <form action="/posts/{{$post->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="">
                    <input type="text"
                        class="block shadow-5xl mb-10 p-2 w-80 italic rounded-md border-gray-300 placeholder-gray-400"
                        name="title" value="{{ $post->title }}">
                </div>
                <div class="">
                    <input type="text"
                        class="block shadow-5xl mb-10 p-2 w-80 italic rounded-md border-gray-300 placeholder-gray-400"
                        name="body" value="{{ $post->body }}">

                    <button type="submit" class="bg-green-500 block shadow-5xl mb-10 p-2 w-80 rounded-md uppercase">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</body>

</html>
