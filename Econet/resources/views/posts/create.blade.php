{{-- <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>

<body>
    <div class="mx-auto w-4/8 py-24">
        <div class="text-center">
            <h1 class="text-5xl uppercase bold">
                Create post
            </h1>
        </div>


        <div class="flex justify-center pt-20">
            <form action="/posts" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="">
                    <input type="file"
                        class="block shadow-5xl mb-10 p-2 w-80 italic rounded-md border-gray-300 placeholder-gray-400"
                        name="image">
                </div>
                <div class="">
                    <input type="text"
                        class="block shadow-5xl mb-10 p-2 w-80 italic rounded-md border-gray-300 placeholder-gray-400"
                        name="title" placeholder="Title">
                </div>
                <div class="">
                    <input type="text"
                        class="block shadow-5xl mb-10 p-2 w-80 italic rounded-md border-gray-300 placeholder-gray-400"
                        name="body" placeholder="body">

                    <button type="submit" class="bg-green-500 block shadow-5xl mb-10 p-2 w-80 rounded-md uppercase">
                        Submit
                    </button>
                </div>
            </form>
            @if ($errors->any())
                <div class="w-4/8 mx-auto text-center">
                    @foreach ($errors->all() as $error)
                        <li class="text-red-500 list-none">
                            {{ $error }}
                        </li>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
</body>

</html>
 --}}

{{--esta solo puede hacerla el admin--}}
 <x-app-layout>
    <div class="mx-auto w-4/8 py-24">
        <div class="text-center">
            <h1 class="text-5xl uppercase bold">
                Create post
            </h1>
        </div>


        <div class="flex justify-center pt-20">
            <form action="/posts" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="">
                    <input type="file"
                        class="block shadow-5xl mb-10 p-2 w-80 italic rounded-md border-gray-300 placeholder-gray-400"
                        name="image">
                </div>
                <div class="">
                    <input type="text"
                        class="block shadow-5xl mb-10 p-2 w-80 italic rounded-md border-gray-300 placeholder-gray-400"
                        name="title" placeholder="Title">
                </div>
                <div class="">
                    <input type="text"
                        class="block shadow-5xl mb-10 p-2 w-80 italic rounded-md border-gray-300 placeholder-gray-400"
                        name="body" placeholder="body">

                    <button type="submit" class="bg-green-500 block shadow-5xl mb-10 p-2 w-80 rounded-md uppercase">
                        Submit
                    </button>
                </div>
            </form>
            @if ($errors->any())
                <div class="w-4/8 mx-auto text-center">
                    @foreach ($errors->all() as $error)
                        <li class="text-red-500 list-none">
                            {{ $error }}
                        </li>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
 </x-app-layout>