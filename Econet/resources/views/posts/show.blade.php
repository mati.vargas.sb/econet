<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>

<body>

    {{-- <div class="mx-auto w-4/5 py-10">
        <div class="text-center mx-auto">
            <h1 class="text-5xl uppercase">
                {{ $post->title }}
            </h1>
        </div>

        <div class="w-5/6 py-10 text-center">
            <span class="uppercase text-blue-500 font-bold text-xs italic">
                Post
            </span>
            <h2 class="text-gray-700 text-5xl hover:text-gray-500">
                <a href="/posts/{{ $post->id }}">
                    {{ $post->title }}
                </a>
            </h2>
            <p class="text-lg text-gray-700 py-6">{{ $post->body }}</p>

            <div>
                <img src="{{ asset('images/' . $post->image_path) }}" alt="">
            </div>

            <hr class="mt-4 mb-8">
        </div>
    </div> --}}

    <!-- component -->
    <!-- This is an example component -->
    <div
        class='flex items-center justify-center min-h-screen from-[#F9F5F3] via-[#F9F5F3] to-[#F9F5F3] bg-gradient-to-br px-2'>
        <div class='w-full max-w-md  mx-auto bg-white rounded-3xl shadow-xl overflow-hidden'>
            <div class='max-w-md mx-auto'>
                <div class='h-[236px]'>
                    <img src="{{ asset('images/' . $post->image_path) }}" alt="">
                </div>
                <div class='p-4 sm:p-6'>
                    <p class='font-bold text-gray-700 text-[22px] leading-7 mb-1'>{{ $post->title }}</p>
                    <p class='text-[#7C7C80] font-[15px] mt-6'>{{ $post->body }}</p>


                    <a target='_blank' href='foodiesapp://food/1001'
                        class='block mt-10 w-full px-4 py-3 font-medium tracking-wide text-center capitalize transition-colors duration-300 transform bg-[#FFC933] rounded-[14px] hover:bg-[#FFC933DD] focus:outline-none focus:ring focus:ring-teal-300 focus:ring-opacity-80'>
                        Prendre un rendez vous
                    </a>
                    <a target='_blank' href="https://apps.apple.com/us/app/id1493631471"
                        class='block mt-1.5 w-full px-4 py-3 font-medium tracking-wide text-center capitalize transition-colors duration-300 transform rounded-[14px] hover:bg-[#F2ECE7] hover:text-[#000000dd] focus:outline-none focus:ring focus:ring-teal-300 focus:ring-opacity-80'>
                        Contactez nous
                    </a>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
