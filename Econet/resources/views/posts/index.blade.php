<x-app-layout>

    <div class="mx-auto w-4/5 py-10">
        <div class="text-center mx-auto">
            <h1 class="text-5xl uppercase">Prestations</h1>
        </div>

        {{-- affichage de posts --}}
        <div class="w-5/6 py-10">
            <h1 class="text-xl font-bold pt-8 pb-2">
                Nos prestations
            </h1>
{{--             aqui deberia hacer las cartas y que sean clickeables para que me envien al show --}}            @foreach ($posts as $post)
                <div class="mx-auto">
                    <span class="uppercase text-blue-500 font-bold text-xs italic">
                        Post
                    </span>
                    <h2 class="text-gray-700 text-5xl hover:text-gray-500">
                        <a href="/posts/{{ $post->id }}">
                            {{ $post->title }}
                        </a>
                    </h2>
                    <p class="text-lg text-gray-700 py-6">{{ $post->body }}</p>
                    <hr>
                </div>
            @endforeach
        </div>
    </div>
</x-app-layout>
