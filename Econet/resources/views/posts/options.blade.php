{{--link para crear nuevo post--}}
<div class="pt-10">
    <a href="posts/create" class="border-b-2 pb-2 border-dotted italic">
        Add new post &rarr;
    </a>
</div>

{{-- link para editar y borrar --}}
<div class="float-right">
    <a class="border-b-2 pb-2 text-green-500 border-dotted italic"
        href="posts/{{ $post->id }}/edit">
        Edit &rarr;
    </a>

    <form class="pt-4" action="posts/{{ $post->id }}" method="POST">
        @csrf
        @method('delete')
        <button type="Submit" class="border-b-2 pb-2 text-red-500 border-dotted italic">
            Delete &rarr;
        </button>
    </form>
</div>