<x-app-layout>
        <div class="max-w-7xl mx-auto">
            @if (session()->has('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            <div class="my-10 flex items-center justify-center">
                <div class="booking-form">
                    <form action="/appointments" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <span class="form-label">
                                        Choisir jour
                                    </span>
                                    <input name="date" class="form-control" type="date" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <span class="form-label">Rdv time</span>
                                    <select name="hour" class="form-control">
                                        @foreach ($hours as $hour)
                                            <option value="{{ $hour }}">
                                                {{ $hour }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <span class="select-arrow"></span>
                                </div>
                            </div>
                            <div class="form-btn col-sm-10">
                                <button class="submit">
                                    submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            {{--   <form action="/appointments" method="POST">
        @csrf
        @livewire('date-time-picker')
        <button wire:click="save">
            submit
        </button>
    </form>  --}}
        </div>
</x-app-layout>
