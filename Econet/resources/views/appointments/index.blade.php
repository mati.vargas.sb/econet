<x-app-layout>

    <div class="mx-auto w-4/5 py-10">
        <div class="text-center mx-auto">
            <h1 class="text-5xl uppercase">rdv</h1>
        </div>

        <div class="pt-10">
            <a href="appointments/create" class="border-b-2 pb-2 border-dotted italic">
                Add new rdv &rarr;
            </a>
        </div>

        {{-- falta verificar que el auth esta autentificado, tira el error pero quiero el msj --}}
        {{-- affichage de rdv --}}
        <div class="w-5/6 py-10">
            <h1 class="text-xl font-bold pt-8 pb-2">
                Rdv of: {{ Auth::user()->name }}
            </h1>
            @foreach (Auth::user()->appointments as $appointment)
                <div class="mx-auto">
                    <div class="float-right">
                        <a class="border-b-2 pb-2 text-green-500 border-dotted italic"
                            href="appointments/{{ $appointment->id }}/edit">
                            Edit &rarr;
                        </a>

                        <form class="pt-4" action="appointments/{{ $appointment->id }}" method="POST">
                            @csrf
                            @method('delete')
                            <button type="Submit" class="border-b-2 pb-2 text-red-500 border-dotted italic">
                                Delete &rarr;
                            </button>
                        </form>
                    </div>
                    <span class="uppercase text-blue-500 font-bold text-xs italic">
                        Appointment
                    </span>
                    <h2 class="text-gray-700 text-5xl hover:text-gray-500">
                        <a href="/appointments/{{ $appointment->id }}">
                            {{ $appointment->datetime }}
                        </a>
                    </h2>
                    <p class="text-lg text-gray-700 py-6">{{ $appointment->datetime }}</p>
                    <hr>
                </div>
            @endforeach
        </div>
    </div>

</x-app-layout>
