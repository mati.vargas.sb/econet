<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Post extends Model
{
    use HasFactory;

    protected $table = 'posts';
    protected $primaryKey = 'id';

    protected $fillable = [
        'title',
        'body',
        'image_path',
        'user_id'
    ];

    /*Hidde or show data 
    
    protected $hidden = [
        'id',
        'body'
    ];

    protected $visible = [
        'title'
    ]; */

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
