<?php

namespace App\Http\Livewire;

use App\Models\Appointment;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Livewire\Component;
use Illuminate\Support\Facades\Auth;

class DateTimePicker extends Component
{

    public string $date;
    public array $availableTimes = [];
    public Collection $appointments;

    public function mount ()
    {

        $this->date = now()->format(format: 'd-m-Y');
        $this->getIntervalsAndAvailableTimes();
        dd($this->getIntervalsAndAvailableTimes());
    }

    public function saveAppointment(string $time)
{
    $appointment = new Appointment([
        'datetime' => Carbon::parse($this->date . ' ' . $time),
        'user_id' => Auth::id(),
    ]);
    $appointment->save();
    $this->getIntervalsAndAvailableTimes();
}
    public function save()
    {
        $date = $this->date;
        dd($date);
        $time = $this->time;
        $availableTimes = $this->availableTimes;
        $appointments = Appointment::create([
            'datetime' => [
                'date',
                'time'
            ],
            'user_id' => Auth::user()->id
        ]);
    } 

    public function updateDate()
    {
        $this->getIntervalsAndAvailableTimes();
    }

    public function render()
    {
        return view('livewire.date-time-picker');
    }

    public function getIntervalsAndAvailableTimes()
    {
        $this->reset('availableTimes');
        $carbonIntervals = Carbon::parse($this->date . '9:00:00')->toPeriod($this->date . '14:00:00', 5, 'hours');
        $this->appointments = Appointment::whereDatetime('date', $this->date)->get();
        
        foreach ($carbonIntervals as $interval) {
            $this->availableTimes[$interval->format('H:i:s')] = !$this->appointments->contains('date', $interval);
        }
        
    } 
}
