<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        /* select * from posts */
        $posts = Post::all()->toJson();
        $posts = json_decode($posts);

        /* dd($posts); */
        return view('posts.index', [
            'posts' => $posts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        /* $post = new Post;
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->save(); */

        /* las reglas de validacion las puse en el objeto createvalidationrequest, y las autorizo
        con true, asi que puedo poner que la autorizacion esta validad como aqui abajo, si cumple las 
        reglas */

        //methods we can use on $request
        //guessExtension
        //getMimeType
        //store()
        //asStore()
        //storePublicly
        //move()
        //getClientOriginalName
        //getClientMimeType
        //guessClientExtension
        //getSize
        //getError()
        //isValid
        /* $test = $request->file('image')->isValid();
        dd($test); */

        

       /*  $request->validate([
            'title'=> 'required|[a-zA-z]',
            'body'=> 'required',
            'image' => 'required'
        ]); */

        $newImageName = uniqid() . '-' . $request->title . '.' . $request->image->extension();
    
        $request->image->move(public_path('images'), $newImageName);

        $post = Post::create([
            'title' => $request->input('title'),
            'body' => $request->input('body'),
            'image_path' => $newImageName,
            'user_id' => Auth::user()->id
        ]);
        

        /* si uso create no necesito usar save() despues.
        Si usara make en vez de create debo usar save() */

        return redirect('/posts');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $post = Post::find($id);

        return view('posts.edit')->with('post', $post);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {

        $post = Post::where('id', $id)
        ->update([
            'title' => $request->input('title'),
            'body' => $request->input('body'),
        ]);

        return redirect('/posts');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Post $post)
    {
        $post->delete();
        return redirect('/posts');

    }

    /* private function storeImage($request) 
    {
        $newImageName = uniqid() . '-' . $request->title . '.' . $request->image->extension();
    
        return $request->image->move(public_path('images'), $newImageName);
    } */
}
