<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

class AppointementController extends Controller
{


    protected $hours = [
        '9:00:00' => '9:00:00',
        '14:00:00' => '14:00:00'
    ];

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $appointments = Appointment::all();
        return view('appointments.index', compact('appointments'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $hours = $this->hours;
        return view('appointments.create', compact('hours'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'date' => 'required',
            'hour' => 'required'
        ]);

        $selectedDay = $request->input('date');
        $selectedHour = $request->input('hour');

        $datetime = Carbon::createFromFormat('Y-m-d H:i:s', "$selectedDay $selectedHour");

        $existingAppointment = Appointment::where('datetime', $datetime)->first();
        if ($existingAppointment) {
            return redirect('/appointments')->with('error', 'Ya hay una cita programada en esta fecha y hora.');
        }

        $appointment = Appointment::create([
            'datetime' => Carbon::now()->setTimeFromTimeString($datetime),
            'user_id' => Auth::user()->id
        ]);

        return redirect('/appointments');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $appointment = Appointment::find($id);
        return view('appointments.show')->with('appointment', $appointment);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $appointment = Appointment::find($id);
        return view('appointments.edit')->with('appointment', $appointment);;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $appointment = Appointment::where('id', $id)
            ->update([
                'datetime' => $request->input('datetime'),
            ]);

        return redirect('/appointments');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Appointment $appointment)
    {
        $appointment->delete();
        return redirect('/appointments');
    }
}
