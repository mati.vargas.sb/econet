<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AppointementController;
use App\Http\Controllers\PostController;


Route::get('/', function () {
    return view('home');
})->name('home');

Route::resource('/posts', PostController::class);

Route::resource('/appointments', AppointementController::class);

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
    ])->group(function () {
        Route::get('/dashboard', function () {
            return view('dashboard');
        })->name('dashboard');
    });
    
    
    








    
    
    
    
    
    

/* Pattern is integer
 
Route::get(
    '/products/{id}',
    [ProductsController::class, 'show']
)->where('id', '[0-9]+');
*/

/*     Patter is string
x
Route::get(
    '/products/{name}/{id}',
    [ProductsController::class, 'show']
)->where([
    'name' => '[a-z]+',
    'id' => '[0-9]+'
]);
*/


/* Route to users


Route::get('/users', function (){
    return 'welcome to the users page';
});

route to users - array(Json)


Route::get('/users', function (){
    return ['php', 'html', 'laravel'];
});

route to users - Json object


Route::get('/users', function (){
    return response()->json([
        'name' => 'mati',
        'course' => 'laravel'
    ]);
});

route to users - function


Route::get('/users', function (){
    return redirect('/');
});
 */

